#!/bin/bash

## Runs the backend and middleend servers on the same pc.
## You can then make requests with the client.

set -e
scriptpath="$(cd "$(dirname "$0")"; pwd -P)"
## Whether to update the git repos or not
update=""

middlename="proxy"
backname="backend"
frontname="frontend"
repos=($backname $middlename $frontname)
BACKEND_PID=
MIDDLEEND_PID=

pushd_() {
    pushd $1 &>/dev/null
}

popd_() {
    popd &>/dev/null
}

# downloads the repos in the parent directory
dl_repos() {
    for repo in ${repos[@]}; do
        if [ ! -d ../$repo ]; then
            git clone "https://gitlab.com/Oxford-Greene/$repo" ../$repo &>/dev/null
        else
            if [ -z "$update" ]; then
                pushd_ "../$repo"
                echo "Updating $repo..."
                git pull &>/dev/null
                popd_
            fi
        fi
    done
}

start_backend() {
    python3 ../$backname/server.py &
    BACKEND_PID=$!
}

start_middleend() {
    python3 ../$middlename/themiddle.py &
    MIDDLEEND_PID=$!
}

kill_servers() {
    echo
    echo "Killing servers..."
    kill $BACKEND_PID
    kill $MIDDLEEND_PID
}

update=$1
cd $scriptpath
dl_repos
trap kill_servers EXIT
start_middleend
start_backend

while true; do
    sleep 100
done

